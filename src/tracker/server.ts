import * as cors from "cors";
import * as express from "express";
import { createServer } from "http";
import { Server } from "net";

const PORT = process.env["TRACKER_PORT"] || 3000;

const app = express();
app.set("port", PORT);
app.use(cors());

// routing
app.get("/test", (req, res) => {
  res.send('Hello there!');
  res.end();
});
app.all("*", (req, res) => res.status(404).send());

const server = createServer(app);

export function startServer(): Server {
  return server.listen(PORT, () => {
    console.log("server listen on port ", PORT);
  });
}

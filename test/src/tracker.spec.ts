import { get, post } from 'request-promise-native';
import { random } from 'faker';
import { expect } from 'chai';
import { dbQuery, syncDB } from './utils/db';

const TRACKER_PORT = process.env["TRACKER_PORT"] || 3000;
const TRACKER_HOST = process.env["TRACKER_HOST"] || "localhost";

describe('Tracker Server', function() {
  this.timeout(40000);
  describe('Movement', function() {
    before(function(done) {
      setTimeout(done, 30000);
    });

    beforeEach(async function() {
      await syncDB()
  
      // reset track event table
      await dbQuery({ type: 'remove', table: 'track_events' });
  
      // create track event data
      this.track = (await dbQuery({
        type: 'insert',
        table: 'track_events',
        values: {
          rider_id: 2,
          north: random.number({ min: 1, max: 20 }),
          west: random.number({ min: 1, max: 20 }),
          east: random.number({ min: 1, max: 20 }),
          south: random.number({ min: 1, max: 20 }),
          createdAt: new Date(),
          updatedAt: new Date()
        },
        returning: [
          'rider_id',
          'north',
          'west',
          'east',
          'south',
          'createdAt'
        ]
      }))[0][0];
    });

    it('harusnya memberikan data movement logs suatu rider', async function() {
      console.log(`http://${TRACKER_HOST}:${TRACKER_PORT}/track`);
      let response;
      try {
        response = await get(`http://${TRACKER_HOST}:${TRACKER_PORT}/movement/${this.track.rider_id}`, { json: true });
      } catch (err) {
        console.log(`Error Occured: ${err}`);
      }
      console.log(response);
      expect(response.ok).to.be.true;
      expect(response.logs).to.be.deep.equal([{
        north: this.track.north,
        west: this.track.west,
        east: this.track.east,
        south: this.track.south,
        time: this.track.createdAt.toISOString()
      }]);
    });
  });
});

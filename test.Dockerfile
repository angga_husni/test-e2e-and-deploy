FROM node:12-alpine 
WORKDIR /app
EXPOSE 80
COPY test/dist /app/test/dist
COPY test/.mocharc.yaml /app/test/
COPY swagger /app/swagger
COPY package.json package-lock.json /app/
RUN npm i
ENTRYPOINT [ "npm", "run" ]
CMD ["test-e2e"]
